require admisc, 2.1.1 # The new one is 2.1.2
require essioc
require busy
require adps4000a, bdfc2ad8

#
#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

#- 10 MB max CA request
epicsEnvSet("ENGINEER", "Luciano C Guedes <luciano.carneiroguedes@ess.eu>")
epicsEnvSet("IOCNAME",      "TS2-010:Ctrl-IOC-011")
epicsEnvSet("AS_TOP", "/opt/nonvolatile")
epicsEnvSet("IOCDIR", "TS2-010_Ctrl-IOC-011")
epicsEnvSet("IOCNAME_SLUG", "$(IOCDIR)")
epicsEnvSet("SETTINGS_FILES","settings")


epicsEnvSet("LOCATION",                     "TS2-Row010")
epicsEnvSet("DEVICE_NAME",                  "Ctrl-PScope-002")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "PICO")
#epicsEnvSet("NUM_SAMPLES",                  "500")
epicsEnvSet("MAX_SAMPLES",                  "100000")
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Autosave configuration
afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/$(SETTINGS_FILES).sav")")

#- create a PicoScope 4000A driver
#- PS4000AConfig(const char *portName, int numSamples, int dataType,
#-               int maxBuffers, int maxMemory, int priority, int stackSize)
#- dataType == NDInt32 == 4
PS4000AConfig("$(PORT)", $(MAX_SAMPLES), 4, 0, 0)

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

dbLoadRecords("ps4000a.template","P=$(PREFIX),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(MAX_SAMPLES)")

#- individual input channels

iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=0, ADDRCH=A0, NAME=A")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=1, ADDRCH=A1, NAME=B")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=2, ADDRCH=A2, NAME=C")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=3, ADDRCH=A3, NAME=D")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=4, ADDRCH=A4, NAME=E")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=5, ADDRCH=A5, NAME=F")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=6, ADDRCH=A6, NAME=G")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=7, ADDRCH=A7, NAME=H")

iocInit()

