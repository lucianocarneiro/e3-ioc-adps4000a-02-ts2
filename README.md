# e3-adps4000a Scope 02 for TS2
ESS Site-specific EPICS module : adps4000a

This repository contains a template for a standard ESS E3 IOC startup script.

## Modules

The startup script loads the standard ESS EPICS modules using the modules:

* admisc
* adps4000a

Those modules shall not be defined in the `st.cmd` file.

The modules required by the IOC shall be added to the startup script (without version):

## Iocsh

Local scripts for conficuring a channel form a Device


## To run the IOC
```bash
source /epics/base-7.0.5/require/3.4.1/bin/setE3Env.bash
```
